--- PACKAGE API REPORT ---

# Init

- [x] Cài package vào project

```
composer require workable/api-report:@dev
```

- [x] Chạy migration

```
php artisan migrate
```

- [x] Đăng ký listener trong config.listeners vào project 

```
  Load listener trong config.listeners vào event trong provider của project
``````