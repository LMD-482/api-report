<?php

use Illuminate\Support\Facades\Route;
use Workable\ApiReport\Http\Controllers\Api\ReportKeywordController;

Route::group([
    'prefix'    => 'api-report',
    'namespace' => 'Workable\ApiReport\Http\Controllers',
], function ()
{
    Route::get('/get-data-keyword-reporter', [ReportKeywordController::class, 'index']);
});