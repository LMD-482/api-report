<?php

namespace Workable\ApiReport\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Workable\ApiReport\Enums\ApiReportEnum;

class WorkerSyncKeywordPublic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'worker:sync-keyword-public';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private  $connectionKeyword;

    public function __construct()
    {
        parent::__construct();
//        $this->connectionJob = config('api-report.database.connection_job');
        $this->connectionKeyword = config('api-report.database.connection_keyword');

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->info('Start sync keyword public');
        $time    = now();
        $dataRaw = $this->getDataLocationPublic();

        foreach ($dataRaw as $key => $value)
        {
            $this->updateData($key, $value, $time);
        }

        $this->info('End sync keyword level');
    }

    private function getDataLocationPublic()
    {
        $this->info('-- Start call api to get data keyword public');
        $dataRaw    = [];
        $arrCountry = ApiReportEnum::$arrCountry;
        $arrUrlMain = ApiReportEnum::$urlCountry;
        foreach ($arrCountry as $country)
        {
            $this->info('Call api to country ' . $country);
            $count             = json_decode(@file_get_contents($arrUrlMain[$country] . 'report-collecter/get-public-keyword'), true);
            if($count == null){
                $count = 0;
            }else{
                if($count['code'] == 1){
                    $count = $count['data']['total'];
                }else{
                    $count = 0;
                }
            }

            $this->info('total: ' . $count);

            $dataRaw[$country] = $count;
        }

        return $dataRaw;
    }

    private function updateData($site, $dataRaw, $time)
    {
        $day   = $time->day;
        $month = $time->month;
        $year  = $time->year;
        $check = DB::connection($this->connectionKeyword)->table('keyword_reporter')
            ->where('site', $site)
            ->where('model', 'keyword_public')
            ->where('month', $month)
            ->where('year', $year)->first();

        if ($check)
        {
            DB::connection($this->connectionKeyword)->table('keyword_reporter')
                ->where('site', $site)
                ->where('model', 'keyword_public')
                ->where('month', $month)
                ->where('year', $year)
                ->update([
                    'd_' . $day => $dataRaw
                ]);
        }
        else
        {
            DB::connection($this->connectionKeyword)->table('keyword_reporter')->insert([
                'site'       => $site,
                'model'      => 'keyword_public',
                'model_type' => 'keyword_public',
                'month'      => $month,
                'year'       => $year,
                'd_' . $day  => $dataRaw
            ]);
        }
    }
}
