<?php

namespace Workable\ApiReport\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Workable\ApiReport\Enums\ApiReportEnum;

class WorkerSyncKeywordLevel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'worker:sync-keyword-level --site=all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $connection;

    public function __construct()
    {
        parent::__construct();
        $this->connection = config('api-report.database.connection_keyword');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $time = now()->subDay();
        $this->info('Start sync keyword level');
        $dataRaw = $this->getDataKeywordLv();
        foreach ($dataRaw as $key => $value)
        {
            foreach ($value as $k => $v)
            {
                $check = DB::connection($this->connection)->table('keyword_reporter')
                    ->where('site', $key)
                    ->where('model', 'keyword_level')
                    ->where('model_type', $k)
                    ->first();

                if (!$check)
                {
                    DB::connection($this->connection)->table('keyword_reporter')->insert([
                        'site'       => $key,
                        'model'      => 'keyword_level',
                        'model_type' => $k,
                        'year'       => $time->year,
                        'month'      => $time->month,
                        'd_1'        => $v,
                    ]);
                    continue;
                }

                DB::connection($this->connection)->table('keyword_reporter')
                    ->where('site', $key)
                    ->where('model', 'keyword_level')
                    ->where('model_type', $k)
                    ->update(['d_1' => $v]);
            }

        }

        $this->info('End sync keyword level');
    }

    private function getDataKeywordLv()
    {
        $dataRaw    = [];
        $arrCountry = ApiReportEnum::$arrCountry;

        foreach ($arrCountry as $country)
        {
            $dataRawCounty = DB::connection($this->connection)->table($country . '_keyword_seos')
                ->select('kws_level', DB::raw('count(*) as total'))
                ->groupBy('kws_level')
                ->get()->toArray();

            foreach ($dataRawCounty as $item)
            {
                $dataRaw[$country]["kw" . $item->kws_level] = $item->total;
            }
        }

        return $dataRaw;
    }
}
