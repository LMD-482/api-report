<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKeywordReporter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('api-report.database.connection_keyword'))->create('keyword_reporter', function (Blueprint $table)
        {
            $table->id();
            $table->string('site');
            $table->string('model');
            $table->string('model_type');
            $table->integer('year');
            $table->integer('month');
            for ($i = 1; $i <= 31; $i++)
            {
                $table->integer('d_' . $i)->default(0);
            }
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(config('api-report.database.connection_keyword'))->dropIfExists('keyword_reporter');
    }
}
