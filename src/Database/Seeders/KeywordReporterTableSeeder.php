<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/21/24
 * Time: 15:18
 */

namespace Workable\ApiReport\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Workable\ApiReport\Enums\ApiReportEnum;

class KeywordReporterTableSeeder extends Seeder
{
    protected $dataType = [
        "keyword_level"    => [
            'kw0', 'kw1', 'kw2', 'kw3', 'kw4', 'kw5'
        ],
        "keyword_public"   => null,
        "location_public"  => null,
        "keyword_extract"  => [
            'count_jd', 'count_jd_done'
        ],
        "location_extract" => [
            'count_jd', 'count_jd_done', 'state', 'city', 'county'
        ]
    ];

    public function run()
    {
        DB::table('keyword_reporter')->truncate();

        $sites     = ApiReportEnum::$arrCountry;
        $timeArray = $this->getTimeArray();

        foreach ($sites as $site)
        {
            foreach ($this->dataType as $key => $value)
            {
                if ($key == "keyword_level")
                {
                    foreach ($value as $model_type)
                    {
                        DB::table('keyword_reporter')->insert([
                            'site'       => $site,
                            'model'      => $key,
                            'model_type' => $model_type,
                            'year'       => Carbon::now()->year,
                            'month'      => Carbon::now()->month,
                            'd_1'        => rand(10000, 30000)
                        ]);
                    }
                }
                if ($key == "keyword_public")
                {
                    foreach ($timeArray as $month => $year)
                    {
                        $data = [
                            'site'       => $site,
                            'model'      => $key,
                            'model_type' => "keyword_public",
                            'year'       => $year,
                            'month'      => $month,
                        ];
                        for ($i = 1; $i < 32; $i++)
                        {
                            $data['d_' . $i] = rand(100, 1000);
                        }
                        DB::table('keyword_reporter')->insert($data);
                    }
                }
                if ($key == "keyword_extract")
                {
                    foreach ($timeArray as $month => $year)
                    {
                        foreach ($value as $model_type)
                        {
                            $arr = [
                                'site'       => $site,
                                'model'      => $key,
                                'model_type' => $model_type,
                                'year'       => $year,
                                'month'      => $month,
                            ];
                            for ($i = 1; $i < 32; $i++)
                            {
                                if ($model_type == 'count_jd')
                                {
                                    $count = rand(100, 150);
                                }
                                else
                                {
                                    $count = rand(50, 100);
                                }
                                $arr['d_' . $i] = $count;
                            }
                            DB::table('keyword_reporter')->insert($arr);
                        }
                    }
                }
                if ($key == "location_extract")
                {
                    foreach ($timeArray as $month => $year)
                    {
                        foreach ($value as $model_type)
                        {
                            $arr = [
                                'site'       => $site,
                                'model'      => $key,
                                'model_type' => $model_type,
                                'year'       => $year,
                                'month'      => $month,
                            ];
                            for ($i = 1; $i < 32; $i++)
                            {
                                if ($model_type == 'count_jd')
                                {
                                    $count = rand(100, 150);
                                }
                                elseif ($model_type == 'count_jd_done')
                                {
                                    $count = rand(50, 100);
                                }
                                elseif ($model_type == 'state')
                                {
                                    $count = rand(30, 50);
                                }
                                elseif ($model_type == 'city')
                                {
                                    $count = rand(30, 50);
                                }
                                else
                                {
                                    $count = rand(30, 40);
                                }
                                $arr['d_' . $i] = $count;
                            }
                            DB::table('keyword_reporter')->insert($arr);
                        }
                    }
                }
            }
        }
    }

    public function getTimeArray($monthBefore = 2): array
    {
        $dataRtn = [];

        for ($i = 0; $i < $monthBefore; $i++)
        {
            $monthCurrent = Carbon::now()->subMonths($i);
            $month        = $monthCurrent->format('m');
            $year         = $monthCurrent->year;

            $dataRtn[$month] = $year;
        }

        return $dataRtn;
    }
}

# php artisan db:seed --class=\\Workable\\ApiReport\\Database\\Seeders\\KeywordReporterTableSeeder