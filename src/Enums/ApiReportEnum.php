<?php

namespace Workable\ApiReport\Enums;

class ApiReportEnum
{
    public static $arrCountry = [
        "us", "uk", "ca", "au", "fr", "it", "de", "nl", "mx", "br", "id", "in", "es", "tr", "se", "fi", "ar", "jp"
    ];

    public static $urlCountry = [
        'us' => 'https://123work.net/',
        'uk' => 'https://uk.123work.net/',
        'ca' => 'https://ca.123work.net/',
        'au' => 'https://au.123work.net/',
        'fr' => 'https://fr.123work.net/',
        'it' => 'https://it.123work.net/',
        'de' => 'https://de.123work.net/',
        'nl' => 'https://nl.123work.net/',
        'mx' => 'https://mx.123work.net/',
        'br' => 'https://br.123work.net/',
        'id' => 'https://id.123work.net/',
        'in' => 'https://in.123work.net/',
        'es' => 'https://es.123work.net/',
        'tr' => 'https://tr.123work.net/',
        'se' => 'https://se.123work.net/',
        'fi' => 'https://fi.123work.net/',
        'ar' => 'https://ar.123work.net/',
        'jp' => 'https://jp.123work.net/',
    ];
}
