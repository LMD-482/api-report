<?php

return [
    'database' => [
        'connection_keyword' => env('REPORT_API_KEYWORD_DB', 'mysql'),
        'connection_job' => env('REPORT_API_JOB_DB', 'mysql'),
    ],

    "listeners" => [
        "analyst_success" => \Workable\ApiReport\Listeners\AnalystSuccessListener::class
    ]
];
