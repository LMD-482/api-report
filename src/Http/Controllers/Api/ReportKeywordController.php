<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/21/24
 * Time: 17:17
 */

namespace Workable\ApiReport\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ApiReport\Enums\ApiReportKeywordEnum;
use Workable\ApiReport\Services\ApiReportKeywordService;
use Workable\ApiReport\Traits\ResponseTrait;

class ReportKeywordController extends Controller
{
    use ResponseTrait;

    private $apiReportKeywordService;

    public function __construct(
        ApiReportKeywordService $apiReportKeywordService
    )
    {
        $this->apiReportKeywordService = $apiReportKeywordService;
    }

    public function index(Request $request)
    {
        $paramQuery   = $request->only(['process', 'site', 'site_id', 'year', 'month', 'day']);
        $process      = $paramQuery['process'] ?? "";
        $arrayProcess = ApiReportKeywordEnum::ARRAY_TYPE;
        if (!in_array($process, $arrayProcess))
        {
            return $this->respondError("process: " . $process . " does not exists.");
        }

        $data = $this->apiReportKeywordService->getDataByType($process, $request);

        return $this->respondSuccess("success", $data);
    }
}