<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/21/24
 * Time: 17:09
 */

namespace Workable\ApiReport\Services;

use Illuminate\Support\Facades\DB;
use Workable\ApiReport\Enums\ApiReportKeywordEnum;
use Workable\Support\Traits\ScopeRepositoryTrait;

class ApiReportKeywordService
{
    use ScopeRepositoryTrait;

    protected $connectionKeyword;
    protected $connectionJob;

    public function __construct()
    {
        $this->connectionKeyword = config('api-report.database.connection_keyword');
        $this->connectionJob     = config('api-report.database.connection_job');
    }

    public function getDataByType($type, $request): array
    {
        $paramQuery = $request->only(['process', 'site', 'site_id', 'year', 'month', 'day']);

        switch ($type)
        {
            case ApiReportKeywordEnum::TYPE_KEYWORD_LEVEL:
                $data = $this->getDataKeywordLevel($paramQuery);
                break;
            case ApiReportKeywordEnum::TYPE_KEYWORD_PUBLIC:
                $data = $this->getDataKeywordPublic($paramQuery);
                break;
            case ApiReportKeywordEnum::TYPE_LOCATION_PUBLIC:
                $data = $this->getDataLocationPublic($paramQuery);
                break;
            case ApiReportKeywordEnum::TYPE_EXTRACT_KEYWORD:
                $data = $this->getDataExtract($paramQuery, "keyword_extract");
                break;
            case ApiReportKeywordEnum::TYPE_EXTRACT_LOCATION:
                $data = $this->getDataExtract($paramQuery, "location_extract");
                break;
            default:
                $data = [];
        }

        return $data;
    }

    public function getDataKeywordLevel($paramQuery): array
    {
        $site = $paramQuery['site'];

        $filter = [
            ["model", "=", ApiReportKeywordEnum::TYPE_KEYWORD_LEVEL]
        ];

        $data    = $this->__getDataKeywordReporter($site, $filter, "get");
        $dataRtn = [];

        foreach ($data as $item)
        {
            $dataRtn[$item->model_type] = $item->d_1;
        }

        return $dataRtn;
    }

    public function getDataKeywordPublic($paramQuery): array
    {
        $site   = $paramQuery['site'];
        $year   = $paramQuery['year'];
        $month  = $paramQuery['month'];
        $day    = $paramQuery['day'];
        $column = "d_" . $day;

        $filter = [
            ["year", "=", $year],
            ["month", "=", $month],
            ["model", "=", ApiReportKeywordEnum::TYPE_KEYWORD_PUBLIC]
        ];

        $data = $this->__getDataKeywordReporter($site, $filter, "first");

        if (!$data)
        {
            return ['total' => 0];
        }

        return ['total' => $data->$column];
    }

    public function getDataLocationPublic($paramQuery): array
    {
        return [];
    }

    public function getDataExtract($paramQuery, $typeExtract): array
    {
        $site   = $paramQuery['site'];
        $year   = $paramQuery['year'];
        $month  = $paramQuery['month'];
        $day    = $paramQuery['day'];
        $column = "d_" . $day;

        $filter = [
            ["year", "=", $year],
            ["month", "=", $month],
            ["model", "=", $typeExtract]
        ];

        $data    = $this->__getDataKeywordReporter($site, $filter, "get");
        $dataRtn = [];

        foreach ($data as $item)
        {
            $dataRtn[$item->model_type] = $item->$column;
        }

        return $dataRtn;
    }

    private function __getDataKeywordReporter($site, $filter = [], $typeGet = "get")
    {
        $query = DB::connection($this->connectionKeyword)->table('keyword_reporter')->where("site", $site);

        if ($filter)
        {
            $query = $this->scopeFilter($query, $filter);
        }

        return ($typeGet == "get") ? $query->get()->toArray() : $query->first();
    }
}