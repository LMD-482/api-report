<?php

namespace Workable\ApiReport;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;
use Workable\ApiReport\Commands\WorkerSyncKeywordLevel;
use Workable\ApiReport\Commands\WorkerSyncKeywordPublic;


class ApiReportServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');
        $this->mergeConfigFrom(__DIR__.'/Configs/config.php', 'api-report');
        $this->commands([
            WorkerSyncKeywordLevel::class,
            WorkerSyncKeywordPublic::class
        ]);

        $this->app->booted(function () {
            $schedule = $this->app->make(Schedule::class);
            $schedule->command('worker:sync-keyword-level')->everyThreeHours();
            $schedule->command('worker:sync-keyword-public')->everyThreeHours();
        });

    }

    public function register()
    {
        parent::register();
    }

    protected function _registerConfig()
    {

    }
}
