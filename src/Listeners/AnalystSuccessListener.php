<?php

namespace Workable\ApiReport\Listeners;


use Illuminate\Support\Facades\DB;

class AnalystSuccessListener
{

    private $connection, $time;

    public function __construct()
    {
        $this->connection = config('api-report.database.connection_keyword');
        $this->time       = now();
    }

    public function handle($event)
    {
        $data        = json_decode($event->dataAnalyst, true);
        $country     = $event->country;
        $dataProcess = $this->mappingData($data, $country);
        $this->processDataMapping($dataProcess);
    }

    private function mappingData($dataAnalyst, $country): array
    {
        $dataTitle = [
            'extract' => $dataAnalyst['title']['extract'],
        ];

        $dataLocation = [
            'state'  => $dataAnalyst['location']['ids']['stateId'] ?? 0,
            'city'   => $dataAnalyst['location']['ids']['cityId'] ?? 0,
            'county' => $dataAnalyst['location']['ids']['countyId'] ?? 0,
        ];

        $site = $country;

        return [
            $dataTitle, $dataLocation, $site
        ];
    }

    private function processDataMapping($data)
    {
        list($dataTitle, $dataLocation, $site) = $data;
        $this->processTitle($dataTitle, $site);
        $this->processLocation($dataLocation, $site);
    }

    private function processTitle($dataTitle, $site)
    {
        $year   = $this->time->year;
        $month  = $this->time->month;
        $dayCol = 'd_' . $this->time->day;

        $checkJd = $this->checkExitsRow(['keyword_reporter', $site, $year, $month, 'keyword_extract', 'jd']);
        if ($checkJd)
        {
            $this->incrementData(['keyword_reporter', $site, $year, $month, 'keyword_extract', 'jd', $dayCol]);
        }
        else
        {
            $this->insertData(['keyword_reporter', $site, $year, $month, 'keyword_extract', 'jd', $dayCol]);
        }

        $checkJdSuccess = $this->checkExitsRow(['keyword_reporter', $site, $year, $month, 'keyword_extract', 'jd_success']);
        if ($checkJdSuccess)
        {
            if ($dataTitle['extract'] == 'success')
            {
                $this->incrementData(['keyword_reporter', $site, $year, $month, 'keyword_extract', 'jd_success', $dayCol]);
            }
        }
        else
        {
            if ($dataTitle['extract'] == 'success')
            {
                $this->insertData(['keyword_reporter', $site, $year, $month, 'keyword_extract', 'jd_success', $dayCol]);
            }
        }

    }

    private function checkExitsRow($arrData)
    {
        list($table, $site, $year, $month, $model, $modelType) = $arrData;
        $check = DB::connection($this->connection)
            ->table($table)
            ->where('year', $year)
            ->where('site', $site)
            ->where('month', $month)
            ->where('model', $model)
            ->where('model_type', $modelType)
            ->first();

        if ($check)
        {
            return true;
        }

        return false;
    }

    private function incrementData($arrData)
    {
        list($table, $site, $year, $month, $model, $modelType, $col) = $arrData;

        DB::connection($this->connection)
            ->table($table)
            ->where('year', $year)
            ->where('site', $site)
            ->where('month', $month)
            ->where('model', $model)
            ->where('model_type', $modelType)
            ->increment($col);
    }

    private function insertData($arrData)
    {
        list($table, $site, $year, $month, $model, $modelType, $col) = $arrData;
        DB::connection($this->connection)
            ->table($table)
            ->insert([
                'year'       => $year,
                'site'       => $site,
                'month'      => $month,
                'model'      => $model,
                'model_type' => $modelType,
                $col         => 1
            ]);
    }

    private function processLocation($dataLocation, $site)
    {
        $year            = $this->time->year;
        $month           = $this->time->month;
        $dayCol          = 'd_' . $this->time->day;
        $arrCheck        = [
            "jd"         => $this->checkExitsRow(['keyword_reporter', $site, $year, $month, 'location_extract', 'jd']),
            "jd_success" => $this->checkExitsRow(['keyword_reporter', $site, $year, $month, 'location_extract', 'jd_success']),
            "state"      => $this->checkExitsRow(['keyword_reporter', $site, $year, $month, 'location_extract', 'state']),
            "city"       => $this->checkExitsRow(['keyword_reporter', $site, $year, $month, 'location_extract', 'city']),
            "county"     => $this->checkExitsRow(['keyword_reporter', $site, $year, $month, 'location_extract', 'county'])
        ];
        $arrCheckAnalyst = [];
        $checkSuccess    = 0;
        foreach ($dataLocation as $key => $value)
        {
            if ($value != null)
            {
                $checkSuccess++;
                $arrCheckAnalyst[$key] = 1;
            }
            else
            {
                $arrCheckAnalyst[$key] = 0;
            }
        }

        foreach ($arrCheck as $key => $value)
        {
            if ($checkSuccess == 0 && $key == 'jd_success')
            {
                continue;
            }
            if ($key != 'jd_success' && $key != 'jd' && $arrCheckAnalyst[$key] == 0)
            {
                continue;
            }
            if ($value)
            {

                $this->incrementData(['keyword_reporter', $site, $year, $month, 'location_extract', $key, $dayCol]);
            }
            else
            {

                $this->insertData(['keyword_reporter', $site, $year, $month, 'location_extract', $key, $dayCol]);
            }
        }
    }
}
